
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BagTests {
    @Test
    public void test_weight() {
        Bag bag = new Bag(12);
        assertEquals(bag.getBagWeight(), 12);
    }
    
}
