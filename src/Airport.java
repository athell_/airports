public class Airport {
    private String name;

    public Airport(String name){
        this.name = name;
    }

    public String getAirportName(){
        return this.name;
    }
    
}
