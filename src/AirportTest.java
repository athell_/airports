
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class AirportTest {
    @Test
    public void test_AirportName() {
        Airport testAirport = new Airport("Test");
        assertEquals(testAirport.getAirportName(), "Test");
    }
    

}