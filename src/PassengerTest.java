import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class PassengerTest {
    @Test
    public void test_name() {
        Passenger testPassenger = new Passenger("Test", "AE383724");
        assertEquals(testPassenger.getPassDetails(), "Test AE383724");
    }

    
}
