public class Passenger {
    private String name;
    private String passportNumber;
    private String passengerDetails;


    public Passenger(String name, String passportNumber){
        this.name = name;
        this.passportNumber = passportNumber;
    }

    public String getPassDetails(){
        passengerDetails= this.name+ " " + this.passportNumber;
        return passengerDetails;
    }
}
