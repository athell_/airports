
public class Bag {
    private int weight;

    
    public Bag(int weight){
        this.weight = weight;
    }

    public int getBagWeight() {
        return this.weight;
    }
}
